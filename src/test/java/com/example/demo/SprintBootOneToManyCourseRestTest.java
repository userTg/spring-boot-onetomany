package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entities.Course;
import com.example.demo.entities.Instructor;
import com.example.demo.repository.CourseRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class SprintBootOneToManyCourseRestTest {

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testGetCoursesByInstructor() {
		Instructor i1 = new Instructor("Clint", "Eastwood", "ce@mail.com");
		
		Course c1 = new Course("Java");
		c1.setCreatedAt(new Date());
		c1.setUpdatedAt(new Date());
		c1.setInstructor(i1);
		
		Course courseSavedInDb = entityManager.persist(c1);
		
		List<Course> courseFromDb = courseRepository.findByInstructorId(i1.getId());
		assertThat(courseFromDb.contains(courseSavedInDb));
	}

	@Test
	void testSave() {
		Instructor i1 = new Instructor("Clint", "Eastwood", "ce@mail.com");
		Course c1 = new Course("Java");
		c1.setCreatedAt(new Date());
		c1.setUpdatedAt(new Date());
		c1.setInstructor(i1);
		Course courseSavedInDb = entityManager.persist(c1);
		Course courseFromDb = courseRepository.getOne(courseSavedInDb.getId());
		assertEquals(courseSavedInDb, courseFromDb);
		assertThat(courseFromDb.equals(courseSavedInDb));
	}

	@Test
	void testUpdateCourse() {
		Instructor i1 = new Instructor("Clint", "Eastwood", "ce@mail.com");
		Course c1 = new Course("Java");
		entityManager.persist(c1);
		c1.setCreatedAt(new Date());
		c1.setUpdatedAt(new Date());
		c1.setInstructor(i1);
		Course getFromDb = courseRepository.getOne(c1.getId());
		getFromDb.setTitle("C#");
		Course courseFromDb = courseRepository.getOne(c1.getId());
		courseFromDb.setTitle("C#");
		assertThat(getFromDb.getTitle().equals(c1.getTitle()));
	}

	@Test
	void testDeleteCourse() {
		Instructor i1 = new Instructor("Clint", "Eastwood", "ce@mail.com");
		Instructor i2 = new Instructor("Steve", "McQueen", "smc@mail.com");
		Course c1 = new Course("Java");
		c1.setCreatedAt(new Date());
		c1.setUpdatedAt(new Date());
		c1.setInstructor(i1);
		Course c2 = new Course("SQL");
		c2.setCreatedAt(new Date());
		c2.setUpdatedAt(new Date());
		c2.setInstructor(i2);
		Course persist = entityManager.persist(c1);
		entityManager.persist(c2);
		entityManager.remove(persist);
		List<Course> allCoursesFromDb = courseRepository.findAll();
		List<Course> courseList = new ArrayList<>();
		for (Course course : allCoursesFromDb) {
			courseList.add(course);
		}
		assertThat(courseList.size()).isEqualTo(1);

	}

}
