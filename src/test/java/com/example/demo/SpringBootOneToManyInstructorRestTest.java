package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.entities.Instructor;

import com.example.demo.repository.InstructorRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class SpringBootOneToManyInstructorRestTest {
	
	@Autowired
	private InstructorRepository instructorRepository;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	void testGetInstructors() {
		Instructor i1 = new Instructor("Clint", "Eastwood", "ce@mail.com");
		Instructor i2 = new Instructor("Steve", "McQueen", "smc@mail.com");
		i1.setCreatedAt(new Date());
		i2.setCreatedAt(new Date());
		i1.setUpdatedAt(new Date());
		i2.setUpdatedAt(new Date());
		
		entityManager.persist(i1);
		entityManager.persist(i2);
		
		List<Instructor> allInstructorsFromDb = instructorRepository.findAll();
		List<Instructor> instructorList = new ArrayList<>();
		for (Instructor instructor : allInstructorsFromDb) {
			instructorList.add(instructor);
		}
	}

	@Test
	void testGetInstructorById() {
		
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com");
		Instructor instructorSavedInDb = entityManager.persist(i1);
		Instructor instructorFromDb = instructorRepository.getOne(instructorSavedInDb.getId());
		assertEquals(instructorSavedInDb, instructorFromDb);
		assertThat(instructorFromDb.equals(instructorSavedInDb));
	}

	@Test
	void testCreateUser() {
		
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com");
		Instructor instructorSavedInDb = entityManager.persist(i1);
		Instructor instructorFromDb = instructorRepository.getOne(i1.getId());
		assertEquals(instructorSavedInDb, instructorFromDb);
		assertThat(instructorFromDb.equals(instructorSavedInDb));
		
		
	}

	@Test
	void testUpdateUser() {
		
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com");
		entityManager.persist(i1);
		Instructor getFromDb = instructorRepository.getOne(i1.getId());
		getFromDb.setLastName("admino");
		Instructor instructorFromDb = instructorRepository.getOne(i1.getId());
		instructorFromDb.setLastName("Morris");
		assertThat(getFromDb.getLastName().equals(i1.getLastName()));
	}

	@Test
	void testDeleteUser() {
		Instructor i1 = new Instructor("Joe", "Dalton", "jd@mail.com");
		Instructor i2 = new Instructor("Luke", "Lucky", "ll@mail.com");
		Instructor persist = entityManager.persist(i1);
		entityManager.persist(i2);
		entityManager.remove(persist);
		List<Instructor> allInstructorsFromDb = instructorRepository.findAll();
		List<Instructor> instructorList = new ArrayList<>();
		for (Instructor instructor : allInstructorsFromDb) {
			instructorList.add(instructor);
		}
		assertThat(instructorList.size()).isEqualTo(1);
	}

}
